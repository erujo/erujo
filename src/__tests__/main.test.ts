import { Erujo, openProject } from '../index';

describe('one file test (1st res)', () => {
  let project: Erujo;
  test('openProject type', () => {
    project = openProject('src/__tests__/res/1/project.yml')
    expect(typeof project).toBe('object');
  });

  test('get diagrams', () => {
    expect(project.getDiagramsList()).toEqual(['diagrams/class.yml']);
  });

  test('get diagram - class', () => {
    //{"elements": [{"height": 100, "obj": {"methods": [{"name": "getName", "return": "String"}, {"args": [{"name": "name", "type": "String"}], "name": "setName"}], "name": "animal", "obj": {"schema": {"methods": "Array"}, "style": {"border": "1px"}, "type": "class"}}, "width": 100, "x": 100, "y": 100}]}
    expect(project.getObject('diagrams/class.yml')).toEqual(['diagrams']);
  });
});
