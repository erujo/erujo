import * as fs from 'fs';
import * as path from 'path';
import * as YAML from 'yaml';
import ref from 'lib-yaml-refs'
import MainDocument from 'lib-yaml-refs/lib/loader';

import { Alias, schema, stringify } from 'yaml/schema'

const getAllFiles = dir =>
  fs.readdirSync(dir).reduce((files, file) => {
    const name = path.join(dir, file);
    const isDirectory = fs.statSync(name).isDirectory();
    return isDirectory ? [...files, ...getAllFiles(name)] : [...files, name];
  }, []);


export class Erujo {
  private loader: MainDocument;
  private project: Document;
  private projectPath: string;
  constructor(filepath: string) {
      const fileLoc = path.resolve(filepath);
      this.projectPath = path.dirname(fileLoc);

      this.loader = new MainDocument({'merge': true, customTags:[ref]});
      this.project = this.loader.getDocument(fileLoc);
      console.log(String(this.project));
  }

  public getDiagramsList():string[] {
    // Create array
    // @ts-ignore
    let include = this.project.getIn(['include']).toJSON();
    let ret: string[] = [];
    // @ts-ignore
    fs.readdirSync(this.projectPath + '/' + include[0]).reduce((files: string[], file: string) => {
      ret.push(path.join(include[0],file));
    }, []);
    return ret;
  }

  public getObject(name: string) {
    return this.loader.getDocument(path.join(this.projectPath, name));
  }
};

export const openProject = function(filepath: string): Erujo {
  return new Erujo(filepath);
}
